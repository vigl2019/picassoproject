package com.example.picassoproject.model;

import java.util.List;

public class PhotoResponse {

    private List<Photo> photos;

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    //==================================================//

    public class Photo {
        private String img_src;

        public String getImgSrc() {
            return img_src;
        }

        public void setImgSrc(String img_src) {
            this.img_src = img_src;
        }
    }
}
