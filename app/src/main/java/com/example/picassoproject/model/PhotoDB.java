package com.example.picassoproject.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class PhotoDB {

    @PrimaryKey
    private int id;

    private String imgSrc;

    public PhotoDB(int id, String imgSrc) {
        this.id = id;
        this.imgSrc = imgSrc;
    }

    //==================================================//

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }
}

