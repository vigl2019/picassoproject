package com.example.picassoproject;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import com.example.picassoproject.model.PhotoDB;

@Database(entities= PhotoDB.class, version = 1)
public abstract class PhotosDataBase extends RoomDatabase {
    public abstract PhotoDao getPhotoDao();
}