package com.example.picassoproject.utils;

import android.content.Context;

import com.example.picassoproject.R;

public class Validator {

    public static String checkTextFieldIsEmpty(Context context, String value) {
        if (value.isEmpty())
            return context.getResources().getString(R.string.empty_text_field);
        else
            return "";
    }

    public static String checkIntegerValueIsPositive(Context context, String value) {

        int intValue = 0;
        String exception = "";

        try {
            intValue = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            exception = e.getMessage();
        }

        if (intValue <= 0 || !exception.isEmpty())
            return context.getResources().getString(R.string.error_integer_value);
        else
            return "";
    }

    //================================================================================//

    public static boolean checkField(String value, Context context) {

        String errorMessage = Validator.checkTextFieldIsEmpty(context, value);

        if (!errorMessage.isEmpty()) {
            Helper.createAlertDialog(errorMessage, null, context);

            return false;
        }

        errorMessage = Validator.checkIntegerValueIsPositive(context, value);

        if (!errorMessage.isEmpty()) {
            Helper.createAlertDialog(errorMessage, null, context);

            return false;
        }

        return true;
    }
}


