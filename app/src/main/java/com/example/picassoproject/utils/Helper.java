package com.example.picassoproject.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.example.picassoproject.ApiService;
import com.example.picassoproject.PhotosDataBase;
import com.example.picassoproject.R;
import com.example.picassoproject.model.PhotoDB;
import com.example.picassoproject.model.PhotoResponse;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class Helper {

    private Context context;
    private Disposable disposable;
    private PhotosDataBase dataBase;

    public Helper() {
    }

    public Helper(Context context, PhotosDataBase dataBase) {
        this.context = context;
        this.dataBase = dataBase;
    }

    //================================================================================//

    public void getPhotosFromServer(String dayNumber, final IHelper iHelper) {

        String apiKey = Helper.getPropertyValue("apiKey", context);

        disposable = ApiService.getPhotos(dayNumber, apiKey)

                .map(new Function<PhotoResponse, List<PhotoDB>>() {
                    @Override
                    public List<PhotoDB> apply(PhotoResponse photoResponse) {

                        final List<PhotoDB> photoDBlist = Converter.convert(photoResponse);
                        dataBase.getPhotoDao().updateAll(photoDBlist);
                        return photoDBlist;
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .filter(new Predicate<List<PhotoDB>>() {
                    @Override
                    public boolean test(List<PhotoDB> photoDBlist) throws Exception {

                        boolean isPhotoDBListHaveValues = ((photoDBlist != null) && (!photoDBlist.isEmpty()));

                        if (!isPhotoDBListHaveValues) {
                            Helper.createAlertDialog(context.getResources().getString(R.string.empty_photo_db_list), null, context);
                        }

                        return isPhotoDBListHaveValues;
                    }
                })


                .subscribe(new Consumer<List<PhotoDB>>() {
                    @Override
                    public void accept(List<PhotoDB> photoDBlist) throws Exception {
                        iHelper.setPhotoDBlist(photoDBlist);
                        iHelper.getPhotos(photoDBlist, 0);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

 //                     Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_LONG).show();
                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                });
    }

    //================================================================================//

    public void getPhotoFromDataBaseByIndex(final int index, final IHelper iHelper) {
        disposable = (dataBase.getPhotoDao().selectAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .filter(new Predicate<List<PhotoDB>>() {
                    @Override
                    public boolean test(List<PhotoDB> photoDBlist) throws Exception {

                        boolean isPhotoDBListHaveValues = ((photoDBlist != null) && (!photoDBlist.isEmpty()));

                        if (!isPhotoDBListHaveValues) {
                            Helper.createAlertDialog(context.getResources().getString(R.string.empty_photo_db_list), null, context);
                        }

                        return isPhotoDBListHaveValues;
                    }
                })

                .subscribe(new Consumer<List<PhotoDB>>() {
                    @Override
                    public void accept(final List<PhotoDB> photoDBlist) throws Exception {
                        iHelper.getPhotos(photoDBlist, index);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

//                      Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_LONG).show();
                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                }));
    }

    //================================================================================//

    public void deleteDataBase(Context context, String dataBaseName) {
        context.deleteDatabase(dataBaseName);
    }

    //================================================================================//

    public void unSubscribe() {

        if (disposable != null && disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    //================================================================================//

    public static String getPropertyValue(String propertyName, Context context) {

        Properties properties = new Properties();
        AssetManager assetManager = context.getAssets();

        try {
            InputStream inputStream = assetManager.open("main.properties");
            properties.load(inputStream);
        } catch (IOException e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        return properties.getProperty(propertyName);
    }

    //================================================================================//

    public static void createAlertDialog(String errorMassage, final View view, Context context) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle("Error of input data!")
                .setMessage(errorMassage)
                .setCancelable(false)
                .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();

                        if (view != null) {
                            view.requestFocus();
                        }
                    }
                });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    //================================================================================//

/*
    public int getIndex(int currentIndex, List<PhotoDB> photoDBlist, String button) {

        if (button.toLowerCase().equals("left")) {
            if (currentIndex != 0) {
                currentIndex--;
            }
        } else {
            if (currentIndex != photoDBlist.size() - 1) {
                currentIndex++;
            }
        }
        return currentIndex;
    }
*/

}



