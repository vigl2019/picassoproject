package com.example.picassoproject.utils;

import com.example.picassoproject.model.PhotoDB;
import java.util.List;

public interface IHelper {
    void getPhotos(List<PhotoDB> photoDBlist, int index);
    void setPhotoDBlist(List<PhotoDB> photoDBlist);
}