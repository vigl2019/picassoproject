package com.example.picassoproject.utils;

import com.example.picassoproject.model.PhotoDB;
import com.example.picassoproject.model.PhotoResponse;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    public static List<PhotoDB> convert (PhotoResponse photoResponse){

        List<PhotoDB> photoDBList = new ArrayList<>();
        String imgScr;

        for(int i = 0; i < photoResponse.getPhotos().size(); i++){
            imgScr = photoResponse.getPhotos().get(i).getImgSrc();
            photoDBList.add(new PhotoDB(i + 1, imgScr));
        }

        return photoDBList;
    }
}

