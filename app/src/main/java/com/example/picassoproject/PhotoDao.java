package com.example.picassoproject;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.picassoproject.model.PhotoDB;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class PhotoDao {

    @Insert
    public abstract void insertAll(List<PhotoDB> photos);

    @Query("SELECT * FROM PhotoDB")
    public abstract Flowable<List<PhotoDB>> selectAll();

    @Query("SELECT * FROM PhotoDB WHERE id = :id")
    public abstract Flowable<PhotoDB> selectPhotoDbById(int id);

    @Query("DELETE FROM PhotoDB")
    public abstract void removeAll();

    public void updateAll(List<PhotoDB> photos) {
        removeAll();
        insertAll(photos);
    }
}

