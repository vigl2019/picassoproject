package com.example.picassoproject;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import com.example.picassoproject.model.PhotoDB;
import com.example.picassoproject.utils.Helper;
import com.example.picassoproject.utils.IHelper;
import com.example.picassoproject.utils.Validator;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private PhotosDataBase dataBase;
    private List<PhotoDB> photos;

    @BindView(R.id.am_day_number)
    EditText dayNumber;

    @BindView(R.id.am_action_get_photos)
    Button actionGetPhotos;

    @BindView(R.id.am_view_photo)
    ImageView viewPhoto;

    private Helper helper;
    private int index = 0;

    private int height;
    private int width;

    private IHelper iHelper = new IHelper() {

        @Override
        public void getPhotos(List<PhotoDB> photoDBlist, int index) {

            Picasso.get()
                    .load(photoDBlist.get(index).getImgSrc())
                    .into(viewPhoto);
        }

        @Override
        public void setPhotoDBlist(List<PhotoDB> photoDBlist){
            photos = photoDBlist;
        }
    };

    //================================================================================//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        //================================================================================//

        viewPhoto.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    // left button
                    if (event.getRawX() < width / 2 && event.getRawY() > height / 2) {
//                      Toast.makeText(MainActivity.this, "Left button click", Toast.LENGTH_LONG).show();

                        if (index != 0) {
                            index--;
                        }
                        else{
                            Helper.createAlertDialog(getResources().getString(R.string.index_out_of_list_size), null, MainActivity.this);
                        }

                        helper.getPhotoFromDataBaseByIndex(index, iHelper);
                    }

                    // right button
                    if (event.getRawX() >= width / 2 && event.getRawY() > height / 2) {
//                      Toast.makeText(MainActivity.this, "Right button click", Toast.LENGTH_LONG).show();

                        if (index != photos.size() - 1) {
                            index++;
                        }
                        else{
                            Helper.createAlertDialog(getResources().getString(R.string.index_out_of_list_size), null, MainActivity.this);
                        }

//                      Toast.makeText(MainActivity.this, "Number of photos: " + String.valueOf(photos.size()), Toast.LENGTH_LONG).show();
                        helper.getPhotoFromDataBaseByIndex(index, iHelper);
                    }
                }

                return true;
            }
        });

        //================================================================================//

        actionGetPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String value = dayNumber.getText().toString().trim();

                if(!Validator.checkField(value, MainActivity.this)){
                    return;
                }

                index = 0;

                helper.getPhotosFromServer(value, iHelper);
            }
        });
    }

    //================================================================================//

    private void init(){

        ButterKnife.bind(this);
        dayNumber.requestFocus();

        dataBase = Room.databaseBuilder(MainActivity.this, PhotosDataBase.class, "PhotosDataBase")
                .fallbackToDestructiveMigration() // удаляет все старые таблицы и создает новые
                .build();

        helper = new Helper(MainActivity.this, dataBase);
        helper.deleteDataBase(MainActivity.this, "PhotosDataBase");

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
    }

    @Override
    protected void onStop() {
        super.onStop();

        Helper helper = new Helper();
        helper.unSubscribe();
    }
}